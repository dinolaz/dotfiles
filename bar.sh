while true; do
DATE=$(echo $(date +"%R"))
VOL=$(echo $(mixer vol | cut -d: -f2))
WEATHER=$(echo $(curl -s https://wttr.in/Dolynska?format=1))
LAYOUT=$(setxkbmap -query | awk '/layout/ {print $2}')
DISK=$(df -h | grep zroot | awk '/home/ {print $5}')
	xsetroot -name " $LAYOUT |  $VOL% | $WEATHER |  $DISK | $DATE"
done 
